package inveniet.query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;

import inveniet.ingest.Ingester;

import org.junit.Test;

/**
 * <p>
 * Tests the Query function which allows the end user to search the memory
 * index by providing AND, OR and NOT terms.
 * </p>
 * <p>
 * The test data used is based on the four files found in resources/querydir
 * directory. The files will contain one or more of the following words. The
 * table outlines which words are found in the which files.
 * <ul>
 * <li>a - adjustment</li>
 * <li>b - baloney</li>
 * <li>c - cacophoney</li>
 * <li>d - dearth</li>
 * <li>e - elegant</li>
 * <li>f - foolhearty</li>
 * <li>g - gentile</li>
 * <li>h - helix</li>
 * <li>i - ignoble</li>
 * </ul>
 * <table border="1">
 * <tr><td>f</td><td>a</td><td>b</td><td>c</td><td>d</td><td>e</td><td>f</td><td>g</td><td>h</td><td>i</td></tr>
 * <tr><td>1</td><td>x</td><td>x</td><td>x</td><td>-</td><td>-</td><td>-</td><td>-</td><td>x</td><td>-</td></tr>
 * <tr><td>2</td><td>x</td><td>x</td><td>-</td><td>x</td><td>-</td><td>-</td><td>x</td><td>x</td><td>-</td></tr>
 * <tr><td>3</td><td>x</td><td>-</td><td>-</td><td>-</td><td>x</td><td>x</td><td>-</td><td>x</td><td>-</td></tr>
 * <tr><td>4</td><td>-</td><td>x</td><td>-</td><td>-</td><td>-</td><td>x</td><td>x</td><td>x</td><td>-</td></tr>
 * </table>
 * </p>
 */
public class QueryTest
{
   private final static String ResourceDir = "/Users/ywhite/Development/playspace/lab/inveniet/src/test/resources/querydir";

   private final static String File1 = ResourceDir + "/query01.txt";
   private final static String File2 = ResourceDir + "/query02.txt";
   private final static String File3 = ResourceDir + "/query03.txt";
   private final static String File4 = ResourceDir + "/query04.txt";
   
   private List<String> results;
   
   /**
    * <p>
    * Runs the indexer for the set of files that are being used for the tests.
    * In addition, initializes variables that are used throughout the test
    * cases.
    * </p>
    * 
    * @throws IOException
    */
   @BeforeClass
   public static void ingest()
   throws IOException
   {
      new Ingester().ingestDirectory(ResourceDir);
   }
   
   @Before
   public void init()
   {
      this.results = new ArrayList<String>();
   }
   
   /**
    * <p>
    * Queries the index for the terms given in the searchString and stores the
    * results in the results instance.
    * </p>
    * 
    * @param searchString the query used to search the index.
    */
   private void query(String searchString)
   {
      this.results.addAll((Collection<String>) new Query().query(searchString));
   }
   
   /**
    * <p>
    * Passes in a null as the query string. Verifies that all results are
    * returned.
    * Construct: null
    * </p>
    */
   public void nullQueryString()
   {
      query(null);
      Assert.assertEquals(4, results.size());
   }
   
   /**
    * <p>
    * Passes in an empty string as the query string. Verifies that all results
    * are returned.
    * Construct: "    "
    * </p>
    */
   public void emptyQueryString()
   {
      query("    ");
      Assert.assertEquals(4, results.size());
   }
  
   /**
    * <p>
    * Tests the AND with an existing term.
    * Construct: a
    * </p>
    */
   @Test
   public void andWithSingleExistingTerm()
   {
      query("adjustment");
      Assert.assertEquals(3, results.size());
      Assert.assertEquals(true, results.contains(File1));
      Assert.assertEquals(true, results.contains(File2));
      Assert.assertEquals(true, results.contains(File3));
   }
   
   /**
    * <p>
    * Tests the AND with a non existing term.
    * Construct: is
    * </p>
    */
   @Test
   public void andWithSingleNonExistingTerm()
   {
      query("ignoble");
      Assert.assertEquals(0, results.size());
   }
  
   /**
    * <p>
    * Tests the AND with a non-existing term.
    * Construct: i
    * </p>
    */
   @Test
   public void andTwoTerms()
   {
      query("adjustment AND baloney");
      Assert.assertEquals(2, results.size());
      Assert.assertEquals(true, results.contains(File1));
      Assert.assertEquals(true, results.contains(File2));
   }
   
   /**
    * <p>
    * Tests the AND with multiple terms.
    * Construct: b AND f AND g
    * </p>
    */
   @Test
   public void andMultipleTerms()
   {
      query("baloney AND foolhearty AND gentile");
      Assert.assertEquals(1, results.size());
      Assert.assertEquals(true, results.contains(File4));
   }
   
   /**
    * <p>
    * Tests the AND with multiple terms but omitting the AND operator. The
    * results should be the same as when using an operator.
    * Construct: b f g ===> b AND f AND g
    * </p>
    */
   @Test
   public void andMultipleTermsSansOperator()
   {
      query("baloney AND foolhearty AND gentile");
      Assert.assertEquals(1, results.size());
      Assert.assertEquals(true, results.contains(File4));
   }
   
   /**
    * <p>
    * Tests three AND terms where the second AND will result in an empty set.
    * Make sure that the third AND doesn't erroneously add results.
    * Construct: c d f  == c AND d AND f
    * </p>
    */
   @Test
   public void andMultipleTermsResultingInEmptySet()
   {
      query("cacophoney dearth foolhearty");
      Assert.assertEquals(0, results.size());
   }
  
   /**
    * <p>
    * A simple OR query with two terms.
    * Construct: c OR d
    * </p>
    */
   @Test
   public void orTwoTerms()
   {
      query("cacophoney OR dearth");
      Assert.assertEquals(2, results.size());
      Assert.assertEquals(true, results.contains(File1));
      Assert.assertEquals(true, results.contains(File2));
   }
   
   /**
    * <p>
    * Test the AND and OR terms together.
    * Construct: a AND b OR e
    * </p>
    */
   @Test
   public void andOrTerms()
   {
      query("adjustment AND baloney OR elegant");
      Assert.assertEquals(3, results.size());
      Assert.assertEquals(true, results.contains(File1));
      Assert.assertEquals(true, results.contains(File2));
      Assert.assertEquals(true, results.contains(File3));
   }
   
   /**
    * <p>
    * Tests the AND, OR and NOT terms together.
    * Construct: a AND b OR f NOT g
    * </p>
    */
   @Test
   public void andOrNotTerms()
   {
      query("adjustment AND baloney OR foolhearty NOT gentile");
      Assert.assertEquals(2, results.size());
      Assert.assertEquals(true, results.contains(File1));
      Assert.assertEquals(true, results.contains(File3));
   }
   
   /**
    * <p>
    * Tests the AND and NOT terms together.
    * Construct: h NOT a
    * </p>
    */
   @Test
   public void andNotTerms()
   {
      query("helix NOT adjustment");
      Assert.assertEquals(1, results.size());
      Assert.assertEquals(true, results.contains(File4));
   }
   
   /**
    * <p>
    * Tests that the NOT will wipe out the AND.
    * Construct: h NOT h
    * </p>
    */
   @Test
   public void andWipedOutBySameNotTerm()
   {
      query("helix NOT helix");
      Assert.assertEquals(0, results.size());
   }

   /**
    * <p>
    * A simple NOT query
    * Construct: NOT a
    * </p>
    */
   public void notSimple()
   {
      query("NOT adjustment");
      Assert.assertEquals(1, results.size());
      Assert.assertEquals(true, results.contains(File4));
   }
   
}
