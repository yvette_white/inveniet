package inveniet.query;

import org.junit.Assert;
import org.junit.Test;

public class OperationTest
{
   /**
    * includes
    * 
    * <p>
    * Basic case.
    * Tests that the basic values in the Operator enum are present and return
    * true.
    * </p>
    */
   @Test
   public void includesExistingProperValues()
   {
      Assert.assertTrue("An argument of 'AND' should return true for includes.", Operator.includes("AND"));
      Assert.assertTrue("An argument of 'OR' should return true for includes.", Operator.includes("OR"));
      Assert.assertTrue("An argument of 'NOT' should return true for includes.", Operator.includes("NOT"));
   }
   
   /**
    * includes
    * 
    * <p>
    * Test that the values exist in the enum but are not upper case.
    * </p>
    */
   @Test
   public void includesExistingButMixedCase()
   {
      Assert.assertFalse("An argument of 'and' should return true for includes.", Operator.includes("and"));
      Assert.assertFalse("An argument of 'And' should return true for includes.", Operator.includes("And"));
      Assert.assertFalse("An argument of 'aNd' should return true for includes.", Operator.includes("aNd"));
      Assert.assertFalse("An argument of 'anD' should return true for includes.", Operator.includes("anD"));
      Assert.assertFalse("An argument of 'ANd' should return true for includes.", Operator.includes("ANd"));
      Assert.assertFalse("An argument of 'aND' should return true for includes.", Operator.includes("aND"));
      
      Assert.assertFalse("An argument of 'or' should return true for includes.", Operator.includes("or"));
      Assert.assertFalse("An argument of 'Or' should return true for includes.", Operator.includes("Or"));
      Assert.assertFalse("An argument of 'oR' should return true for includes.", Operator.includes("oR"));
      
      Assert.assertFalse("An argument of 'not' should return true for includes.", Operator.includes("not"));
      Assert.assertFalse("An argument of 'Not' should return true for includes.", Operator.includes("Not"));
      Assert.assertFalse("An argument of 'nOt' should return true for includes.", Operator.includes("nOt"));
      Assert.assertFalse("An argument of 'noT' should return true for includes.", Operator.includes("noT"));
      Assert.assertFalse("An argument of 'NOt' should return true for includes.", Operator.includes("NOt"));
      Assert.assertFalse("An argument of 'nOT' should return true for includes.", Operator.includes("nOT"));
   }
   
   /**
    * includes
    * 
    * <p>
    * Tests that values that are non-existent in the Operator enum return
    * false.
    * </p>
    */
   @Test
   public void includesNonExistent()
   {
      Assert.assertFalse("An argument of 'boo' should return false for includes.", Operator.includes("boo"));
      Assert.assertFalse("An argument of 'NOTS' should return false for includes.", Operator.includes("NOTS"));
   }
}
