package inveniet.ingest;

import inveniet.index.MemoryIndex;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class IngesterTest
{
   private final static String ResourceDir = "/Users/ywhite/Development/playspace/lab/inveniet/src/test/resources/ingesterdir";
   private final static String NestedDir = "/Users/ywhite/Development/playspace/lab/inveniet/src/test/resources/lyrics";

   @Before
   public void clear()
   {
      MemoryIndex.getInstance().clear();
   }
   
   /**
    * ingestFile
    * 
    * <p>
    * Tests that an exception is thrown if the filename parameter is an empty
    * string.
    * </p>
    */
   @Test
   public void ingestFileEmptyFilename()
   {
      try
      {
         new Ingester().ingestFile("  ");
         Assert.fail("The file ingesting should throw an exception if the file name is an empty string.");
      }
      catch (IOException e)
      {
      }
   }
   
   /**
    * ingestFile
    * 
    * <p>
    * Tests that an exception is thrown if the filename parameter is null.
    * </p>
    */
   @Test
   public void ingestFileNullFilename()
   {
      try
      {
         new Ingester().ingestFile(null);
         Assert.fail("The file ingesting should throw an exception if the file name is null.");
      }
      catch (IOException e)
      {
      }
   }
   
   /**
    * ingestFile
    * 
    * <p>
    * Tests that an exception is thrown if the filename given does not exist.
    * </p>
    */
   @Test
   public void ingestFileNonExistent()
   {
      try
      {
         String filename = ResourceDir + "hhhhhhhhhhh.txt";
         new Ingester().ingestFile(filename);
         Assert.fail("The file ingesting should throw an exception because the file " + filename + " does not exist.");
      }
      catch (IOException e)
      {
      }
   }

   /**
    * ingestFile
    * 
    * <p>
    * Uses a simple file that does not contain punctuation or multiple lines.
    * </p>
    */
   @Test
   public void ingestFileSimple()
   {
      try
      {
         Ingester ingester = new Ingester();
         ingester.ingestFile(ResourceDir + "/testfile03.txt");

         Assert.assertEquals(3, MemoryIndex.getInstance().size());
         Assert.assertEquals(1, MemoryIndex.getInstance().lookupTerm("one").size());
         Assert.assertEquals(1, MemoryIndex.getInstance().lookupTerm("two").size());
         Assert.assertEquals(1, MemoryIndex.getInstance().lookupTerm("three").size());
      } 
      catch (Exception e)
      {
         Assert.fail();
      }
   }

   /**
    * ingestFile
    * 
    * <p>
    * Uses a simple file that contains punctuation and multiple lines. This case
    * is more realistic. We expect only the words to be pulled in and not the
    * punctuation.
    * </p>
    */
   @Test
   public void ingestFileWithPunctuation()
   {
      try
      {
         Ingester ingester = new Ingester();
         ingester.ingestFile(ResourceDir + "/testfile04.txt");

         Assert.assertEquals(3, MemoryIndex.getInstance().size());
         Assert.assertEquals(1, MemoryIndex.getInstance().lookupTerm("one").size());
         Assert.assertEquals(1, MemoryIndex.getInstance().lookupTerm("two").size());
         Assert.assertEquals(1, MemoryIndex.getInstance().lookupTerm("three").size());
      } catch (Exception e)
      {
         Assert.fail();
      }
   }
   
   /**
    * ingestDirectory
    * 
    * <p>
    * Tests that an exception is thrown if the directory name parameter is an
    * empty string.
    * </p>
    */
   @Test
   public void ingestDirectoryEmptyFilename()
   {
      try
      {
         new Ingester().ingestDirectory("  ");
         Assert.fail("The directory ingesting should throw an exception if the directory name is an empty string.");
      }
      catch (IOException e)
      {
      }
   }
   
   /**
    * ingestDirectory
    * 
    * <p>
    * Tests that an exception is thrown if the directory name parameter is null.
    * </p>
    */
   @Test
   public void ingestDirectoryNullFilename()
   {
      try
      {
         new Ingester().ingestDirectory(null);
         Assert.fail("The directory ingesting should throw an exception if the directory name is null.");
      }
      catch (IOException e)
      {
      }
   }
   
   /**
    * ingestDirectory
    * 
    * <p>
    * Tests that an exception is thrown if the directory name given does not exist.
    * </p>
    */
   @Test
   public void ingestDirectoryNonExistent()
   {
      try
      {
         String filename = ResourceDir + "/hhhhhhhhhhh";
         new Ingester().ingestDirectory(filename);
         Assert.fail("The directory ingesting should throw an exception because the direcotry " + filename + " does not exist.");
      }
      catch (IOException e)
      {
      }
   }
   
   /**
    * ingestDirectory
    * 
    * <p>
    * Tests that an exception is thrown if the directory name given is a file.
    * </p>
    */
   @Test
   public void ingestDirectoryDirectoryIsaFile()
   {
      try
      {
         String filename = ResourceDir + "/testfile01.txt";
         new Ingester().ingestDirectory(filename);
         Assert.fail("The directory ingesting should throw an exception because the directory given is a file: " + filename + ".");
      }
      catch (IOException e)
      {
      }
   }

   /**
    * <p>
    * Tests that all the files are ingested and represented in the MemoryIndex
    * (assuming that each file has at least one term). This test contains
    * multiple files but does not contain nested directories.
    * </p>
    */
   @Test
   public void ingestDirectoryFlatDirectory()
   {
      try
      {
         new Ingester().ingestDirectory(ResourceDir);
         Assert.assertEquals(4, MemoryIndex.getInstance().values().size());
      } catch (Exception e)
      {
         Assert.fail();
      }
   }
   
   /**
    * <p>
    * Tests that all the files are ingested and represented in the MemoryIndex
    * (assuming that each file has at least one term). This test contains
    * multiple files in nested directories.
    * </p>
    */
   @Test
   public void ingestDirectoryNestedDirectories()
   {
      try
      {
         new Ingester().ingestDirectory(NestedDir);
         Assert.assertEquals(4, MemoryIndex.getInstance().values().size());
      } catch (Exception e)
      {
         Assert.fail();
      }
   }
}