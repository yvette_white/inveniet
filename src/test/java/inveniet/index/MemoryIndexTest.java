package inveniet.index;

import com.google.common.collect.ImmutableSet;

import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MemoryIndexTest
{
   @Before
   public void clear()
   {
      MemoryIndex.getInstance().clear();
   }
   
   /**
    * addTerm
    * 
    * <p>
    * A basic test to determine if the addTerm method is properly adding terms
    * and associating them with the file names given.
    * </p>
    * 
    * @throws Exception
    */
	@Test
	public void addTermBasicTest() throws Exception
	{
		MemoryIndex.getInstance().addTerm("term1", "doc1");
		MemoryIndex.getInstance().addTerm("term2", "doc1");
		MemoryIndex.getInstance().addTerm("term3", "doc1");
		MemoryIndex.getInstance().addTerm("term4", "doc1");
		MemoryIndex.getInstance().addTerm("term2", "doc2");
		MemoryIndex.getInstance().addTerm("term2", "doc3");
		MemoryIndex.getInstance().addTerm("term3", "doc2");
		
		ImmutableSet<String> docIds = new ImmutableSet.Builder<String>().add("doc1", "doc2", "doc3")
                                                                        .build();
        Set<String> docs = MemoryIndex.getInstance().lookupTerm("term2");
        Assert.assertEquals(docIds.size(), docs.size());
        for(String docId : docIds)
        {
            Assert.assertTrue(docs.contains(docId));
        }
	}
	
	/**
	 * addTerm
	 * 
	 * <p>
	 * Tests that a given term that is an empty string is not added to the
	 * index.
	 * </p>
	 */
	@Test
	public void addTermWithEmptyTermParam()
	{
	   MemoryIndex.getInstance().addTerm("   ", "fake.txt");
	   Assert.assertEquals(0, MemoryIndex.getInstance().size());
	}
   
   /**
    * addTerm
    * 
    * <p>
    * Tests that a given term that is null is not added to the index.
    * </p>
    */
	@Test
   public void addTermWithNullTermParam()
   {
      MemoryIndex.getInstance().addTerm(null, "fake.txt");
      Assert.assertEquals(0, MemoryIndex.getInstance().size());
   }
}
