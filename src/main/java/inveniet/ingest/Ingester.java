package inveniet.ingest;

import inveniet.index.MemoryIndex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * This class will take care of ingesting files into the index.
 */
public class Ingester
{
   public Ingester()
   {
   }

   /**
    * <p>
    * Ingest a directory of files into the index making sure to ingest all
    * subdirectories as well.
    * </p>
    * 
    * @param dirname a string representing the absolute path of the directory
    * to ingest.
    * @throws IOException if there is an error accessing or parsing the file;
    * if the dirname is missing; if the dirname is not a directory.
    */
   public void ingestDirectory(String dirname) throws IOException
   {
      if (dirname == null || dirname.trim().isEmpty())
      {
         throw new IOException("The directory name is required.");
      }

      File directory = new File(dirname);
      if (!directory.isDirectory())
      {
         throw new IOException("The path given is not a directory.");
      }

      for (String filename : directory.list())
      {
         File file = new File(dirname + File.separator + filename);
         if (file.isDirectory())
         {
            ingestDirectory(file.getCanonicalPath());
         } 
         else
         {
            ingestFile(file);
         }
      }
   }

   /**
    * <p>
    * Ingest the given file into the index.
    * </p>
    * 
    * @param filename a string representing the absolute path of the file to
    *           ingest
    * @throws IOException if there is an error with the file or parsing the
    *            file; if filename is missing.
    */
   public void ingestFile(String filename) throws IOException
   {
      if (filename == null || filename.trim().isEmpty())
      {
         throw new IOException("The file name is required.");
      }
      this.ingestFile(new File(filename));
   }

   /**
    * <p>
    * Ingest the given file into the index.
    * </p>
    * 
    * @param   file the file to ingest. required.
    * @throws  IOException if an error occurs while accessing or parsing the
    *            file.
    */
   private void ingestFile(File file) throws IOException
   {
      if (shouldSkip(file))
      {
         return;
      }

      Scanner scanner = null;
      try
      {
         scanner = new Scanner(new BufferedReader(new FileReader(file)));
         scanner.useDelimiter("\\W");

         while (scanner.hasNext())
         {
            MemoryIndex.getInstance().addTerm(normalize(scanner.next()), file.getPath());
         }
      } 
      finally
      {
         if (scanner != null)
         {
            scanner.close();
         }
      }
   }

   /**
    * <p>
    * Indicates whether or not the file should be skipped.
    * </p>
    * 
    * @param file the file being investigated for skipping. Required.
    * @return true if this file should be skipped and false if not.
    * @throws IOException if an error occurred when accessing the file.
    */
   private boolean shouldSkip(File file) throws IOException
   {
      return file.isHidden();
   }
   
   /**
    * <p>
    * A quick normalization of a string by making the string lower case and
    * removing white spaces.
    * </p>
    * 
    * @param   str the string to normalize. required.
    */
   private String normalize(final String str)
   {
      return str.toLowerCase().trim();
   }
}
