package inveniet.query;

import inveniet.index.MemoryIndex;

import java.util.Set;
import java.util.TreeSet;

public class Query
{
   private Set<String> results;
   private Operator operation;
   private boolean firstTerm = true; 
   
   /**
    * <p>
    * This will query the index and return the documents that match the query.
    * </p>
    * <p>
    * Query String will have foo AND bar OR biz NOT baz The query will be parsed
    * from left to right. The following stop words (in all caps) will be defined
    * as follows:
    * <ul>
    * <li>AND - will only keep document ids where both term 1 and term 2
    * appear</li>
    * <li>OR - will keep documents where either term 1 or term 2 appear</li>
    * <li>NOT - will keep documents which have term 1 but not term 2.</li>
    * </ul>
    * <p>
    * Example: term1 AND term2 OR term3 NOT term4 This will keep all document
    * ids that have both term1 and term2. It will then keep all document ids
    * which appear in the previous set (term1 AND term2) or documents which
    * term3 appear in. It will then keep all the document ids from the previous
    * part of the query and remove all document ids from that set which term4
    * appears in.
    * </p>
    * 
    * @param query the query string that provides the search criteria.
    * @return an iterable of document ids which match the query. an empty
    *         iterable is returned if nothing is found.
    */
   public Iterable<String> query(String query)
   {
      reset();
      search(query);
      return this.results;
   }

   /**
    * <p>
    * Resets the query results and prepares for additional processing.
    * </p>
    */
   private void reset()
   {
      this.results = new TreeSet<String>();
      this.operation = Operator.AND;
      this.firstTerm = true;
   }

   /**
    * <p>
    * Parses the query string that is provided and searches the memory index
    * with the search criteria provided.
    * </p>
    * <p>
    * The assumption is that the query operations are processed as they are
    * encountered.
    * </p>
    */
   private void search(String query)
   {
      if (query == null || query.trim().isEmpty())
      {
         this.results.addAll(MemoryIndex.getInstance().values());
         return;
      }

      for (String criterion : query.split("\\W"))
      {
         if (Operator.includes(criterion))
         {
            this.operation = Operator.valueOf(criterion);
         }
         else
         {
            process(criterion);
         }
      }
   }

   /**
    * <p>
    * Searches for the given term in the index and adjusts the results
    * accordingly.
    * </p>
    * <p>
    * The assumption is that AND terms will appear before OR terms which
    * will appear before NOT terms.
    * </p>
    * 
    * @param term the term to search for in the index. required.
    */
   private void process(String term)
   {
      Set<String> termMatches = MemoryIndex.getInstance().lookupTerm(term);
      if (termMatches != null)
      {
         if (this.operation.equals(Operator.AND))
         {
            if (this.firstTerm)
            {
               this.results.addAll(termMatches);
            }
            else
            {
               this.results.retainAll(termMatches);
            }
         }
         else if (this.operation.equals(Operator.OR))
         {
            this.results.addAll(termMatches);
         }
         else
         {
            if (this.firstTerm)
            {
               this.results.addAll(MemoryIndex.getInstance().values());
            }
            this.results.removeAll(termMatches);
         }
      }
      this.firstTerm = false;
   }
}
