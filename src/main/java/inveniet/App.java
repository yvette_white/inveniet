package inveniet;

import inveniet.ingest.Ingester;
import inveniet.query.Query;

import java.io.IOException;
import java.util.Scanner;

/**
 * <p>
 * Searches a directory or file and indexes the terms in the enclosed files.
 * Allows the user to to query for the terms.
 * </p>
 */
public class App
{
   private static Ingester ingester = new Ingester();
   private static Query query = new Query();

   public static void main(String[] args) throws Exception
   {
      Scanner inputScanner = new Scanner(System.in);
      printHelp();
      while (true)
      {
         System.out.println("Enter Command: ");
         String command = inputScanner.nextLine();
         handleCommand(command);
      }
   }

   /**
    * <p>
    * Prints to standard out the command syntax for using this application.
    * </p>
    */
   private static void printHelp()
   {
      System.out.println("type in <command>: <arguments>");
      System.out.println("id: <absolute directory path>");
      System.out.println("if: <absolute file  path>");
      System.out.println("q: <query terms>");
      System.out.println("x will exit the program");
   }

   /**
    * <p>
    * Given a command by the user, interprets what was requested and handles the
    * request.
    * </p>
    * 
    * @param command the command provided by the user to give instructions on
    *           what ingesting and searching is being requested. required.
    * @throws Exception if an error occurs while processing the request.
    */
   private static void handleCommand(String command) throws Exception
   {
      if (command.trim().equalsIgnoreCase("x"))
      {
         System.exit(0);
      }

      String[] parts = command.split(":");
      if (parts.length < 2)
      {
         throw new RuntimeException("Invalid command format");
      }

      String cmd = normalize(parts[0]);
      String arg = parts[1].trim();

      if (cmd.equalsIgnoreCase("id"))
      {
         ingestDirectory(arg);
      }
      else if (cmd.equalsIgnoreCase("if"))
      {
         ingestFile(arg);
      }
      else if (cmd.equalsIgnoreCase("q"))
      {
         Iterable<String> results = query.query(arg);
         for (String result : results)
         {
            System.out.println(result);
         }
      }
      else
      {
         printHelp();
      }
   }
   
   /**
    * <p>
    * Calls the processing for ingesting the given directory. If an exception
    * is thrown then an error message is printed to system out but processing
    * is not halted.
    * </p>
    * 
    * @param dirname the absolute path name of the directory to ingest.
    */
   private static void ingestDirectory(String dirname)
   {
      long startms = new java.util.Date().getTime();
      try
      {
         ingester.ingestDirectory(dirname);
      }
      catch (IOException e)
      {
         System.out.println(">>  " + e.getMessage());
      }
      long endms = new java.util.Date().getTime();
      System.out.println("Total processing time (ms): " + (endms - startms));
   }
   
   /**
    * <p>
    * Calls the processing for ingesting the given file. If an exception is
    * thrown then an error message is printed to system out but processing is
    * not halted.
    * </p>
    * 
    * @param filename the absolute file name of the file to ingest.
    */
   private static void ingestFile(String arg)
   {
      try
      {
         ingester.ingestDirectory(arg);
      }
      catch (IOException e)
      {
         System.out.println(">>  " + e.getMessage());
      }
   }

   /**
    * <p>
    * Normalizes the given string by converting it to lower case and trimming
    * off preceding and succeeding white spaces.
    * </p>
    * 
    * @param str the string to normalize. required.
    * @return the normalized version of the given string.
    */
   private static String normalize(String str)
   {
      return str.toLowerCase().trim();
   }
}
