package inveniet.index;

import com.google.common.collect.SetMultimap;
import com.google.common.collect.HashMultimap;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class MemoryIndex
{
   private SetMultimap<String, String> index;

   /**
    * <p>
    * Get a instance of the database.
    * </p>
    * 
    * @return a instance of the database
    */
   public static MemoryIndex getInstance()
   {
      return MemoryIndexHolder.INSTANCE;
   }

   /**
    * <p>
    * Add a term and a document to the index. Empty strings and nulls are
    * discarded.
    * </p>
    * 
    * @param term a string to represent the term
    * @param docId the filename from which the term came from
    */
   public void addTerm(final String term, final String docId)
   {
      if (term == null || term.trim().isEmpty()) return;
      index.put(term, docId);
   }

   /**
    * <p>
    * Fetches a single term from the index.
    * </p>
    * 
    * @param term the term is being search for in the index.
    * @return a set of docIds which match the term or null if the term is not in
    *         the index
    */
   public Set<String> lookupTerm(String term)
   {
      if (!index.containsKey(term))
      {
         return null;
      }

      return index.get(term);
   }

   /**
    * <p>
    * Prints to the system output a detailed representation of the index.
    * </p>
    */
   public void print()
   {
      Iterator<String> i = this.index.keys().iterator();
      while (i.hasNext())
      {
         String key = i.next();
         System.out.print("key = " + key);
         System.out.println("  values = " + this.index.get(key));
      }
   }

   /**
    * <p>
    * Clears the index; all entries are removed.
    * </p>
    */
   public void clear()
   {
      this.index.clear();
   }

   /**
    * <p>
    * Returns the number of terms found in the index.
    * </p>
    * 
    * @return The number of terms, or size, in the index.
    */
   public int size()
   {
      return this.index.size();
   }

   /**
    * <p>
    * Returns a set of all values referenced within the index. Because a set is
    * returned, no duplicates values are included.
    * </p>
    * 
    * @return A set of all values referenced within the index.
    */
   public Set<String> values()
   {
      return new TreeSet<String>(this.index.values());
   }

   private MemoryIndex()
   {
      this.index = HashMultimap.create();
   }

   private static class MemoryIndexHolder
   {
      private static final MemoryIndex INSTANCE = new MemoryIndex();
   }

}
